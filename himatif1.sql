-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 12 Jul 2020 pada 20.14
-- Versi Server: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `himatif1`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `bph`
--

CREATE TABLE `bph` (
  `bph_id` int(11) NOT NULL,
  `periode_id` int(11) DEFAULT NULL,
  `mahasiswa_id` int(11) DEFAULT NULL,
  `jabatan` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bph`
--

INSERT INTO `bph` (`bph_id`, `periode_id`, `mahasiswa_id`, `jabatan`) VALUES
(3, 1, 1, 'Ketua'),
(5, 1, 14, 'Bendahara'),
(6, 1, 13, 'Sekretaris'),
(7, 4, 15, 'Ketua Himpunan'),
(8, 4, 16, 'Wakil Ketua'),
(9, 4, 18, 'Bendahara'),
(10, 4, 17, 'Sekretaris'),
(11, 1, 19, 'Wakil Ketua'),
(13, 7, 18, 'Koordinator'),
(14, 3, 13, 'ketua');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kegiatan`
--

CREATE TABLE `kegiatan` (
  `kegiatan_id` int(11) NOT NULL,
  `bph_id` int(11) DEFAULT NULL,
  `periode_id` int(11) DEFAULT NULL,
  `nama_kegiatan` varchar(100) DEFAULT NULL,
  `waktu_kegiatan` date DEFAULT NULL,
  `tempat_kegiatan` varchar(100) DEFAULT NULL,
  `deskripsi_kegiatan` text,
  `foto_kegiatan` varchar(255) NOT NULL,
  `tanggal_berakhir` date NOT NULL,
  `status_kegiatan` varchar(100) NOT NULL,
  `penulis` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kegiatan`
--

INSERT INTO `kegiatan` (`kegiatan_id`, `bph_id`, `periode_id`, `nama_kegiatan`, `waktu_kegiatan`, `tempat_kegiatan`, `deskripsi_kegiatan`, `foto_kegiatan`, `tanggal_berakhir`, `status_kegiatan`, `penulis`) VALUES
(1, 3, 1, 'Perpisahan 2016', '2020-01-14', 'Holbung', 'Kegiatan dilaksanakan di Open Teather bersama semua angkatan							', 'IMG_2995.JPG', '2020-06-24', 'Terlaksanakan', 'Hernan'),
(26, 3, 1, 'Wisuda', '2020-09-09', 'Institut Teknologi Del', 'Wisuda Angkatan 2017 D3 Teknologin Informasi 				', 'gambar1.jpg', '2020-09-09', 'Terlaksanakan', 'Hernan'),
(27, 3, 1, 'Pembukaan Semester Baru', '2020-09-23', 'Gedung Serba Guna  (GSG)', 'Program Studi Teknologi Informasi Diploma Tiga membekali mahasiswa dengan keterampilan dan pengetahuan di bidang Teknik Informasi seperti Software Engineering, Object-Oriented Analysis and Design, Software Testing, Manajemen Proyek dan pembangunan perangkat lunak.', 'kamar.jpg', '2020-09-23', 'Belum Terlaksanakan', 'Hernan Crespo Panjaitan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kuisioner`
--

CREATE TABLE `kuisioner` (
  `kuisioner_id` int(11) NOT NULL,
  `bph_id` int(11) DEFAULT NULL,
  `kegiatan_id` int(11) DEFAULT NULL,
  `mahasiswa_id` int(11) DEFAULT NULL,
  `jab1` varchar(200) NOT NULL,
  `jab2` varchar(200) NOT NULL,
  `jab3` varchar(200) NOT NULL,
  `jab4` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kuisioner`
--

INSERT INTO `kuisioner` (`kuisioner_id`, `bph_id`, `kegiatan_id`, `mahasiswa_id`, `jab1`, `jab2`, `jab3`, `jab4`) VALUES
(1, 3, 1, 0, '', '', '', ''),
(2, NULL, NULL, 0, '', '', '', ''),
(3, 6, 26, 0, '', '', '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kuisioner_kegiatan`
--

CREATE TABLE `kuisioner_kegiatan` (
  `kuisioner_id` int(11) NOT NULL,
  `kegiatan_id` int(11) NOT NULL,
  `mahasiswa_id` int(11) NOT NULL,
  `jab1` varchar(225) NOT NULL,
  `jab2` varchar(225) NOT NULL,
  `jab3` varchar(225) NOT NULL,
  `jab4` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kuisioner_kegiatan`
--

INSERT INTO `kuisioner_kegiatan` (`kuisioner_id`, `kegiatan_id`, `mahasiswa_id`, `jab1`, `jab2`, `jab3`, `jab4`) VALUES
(1, 1, 1, 'Tidak', 'Tidak', 'Ya', 'asdf'),
(2, 26, 1, 'Ya', 'Tidak', 'Ya', 'bagus'),
(3, 1, 1, 'Ya', 'Ya', 'Ya', 'keren');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `mahasiswa_id` int(11) NOT NULL,
  `nama` text,
  `username` varchar(225) NOT NULL,
  `password` varchar(225) NOT NULL,
  `nim` varchar(100) DEFAULT NULL,
  `kelas` text,
  `angkatan` varchar(100) DEFAULT NULL,
  `alamat` varchar(225) NOT NULL,
  `email` varchar(225) NOT NULL,
  `status_mahasiswa` varchar(225) NOT NULL,
  `tempat_lahir` varchar(225) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `role` varchar(100) NOT NULL,
  `foto_mahasiswa` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mahasiswa`
--

INSERT INTO `mahasiswa` (`mahasiswa_id`, `nama`, `username`, `password`, `nim`, `kelas`, `angkatan`, `alamat`, `email`, `status_mahasiswa`, `tempat_lahir`, `tanggal_lahir`, `role`, `foto_mahasiswa`) VALUES
(1, 'Hernan Crespo Panjaitan', 'hernan', 'hernan', '11317056', '32TI2', '2017', 'JL.Bahkora II Bawah PematangSiantar								', 'if318056@itdel', 'aktif', 'DolokSanggul', '1999-07-12', 'admin', '421c8108cfac759d262052bf103b71ea.jpg'),
(13, 'Romauli Feronica Siregar', 'romauli', 'romauli', '11317065', '33TI2', '2017', 'Sidikalang		', 'if317065@students.del.ac.id', 'aktif', 'Parongil', '1998-07-12', 'user', 'IMG_3110.JPG'),
(14, 'Layla Hafni Ainun Hutasuhut', 'layla', 'layla', '11317061', '33TI2', '2017', 'Sigumpar, Tobasa, Sumatera Utara		', 'if317061@students.del.ac.id', 'aktif', 'Lumban Binanga', '1999-07-22', 'user', 'IMG_3074.JPG'),
(15, 'Hagai Sitanggang', 'hagai', 'hagai', '11317048', '33TI2', '2017', '			Pematang Siantar		', 'if317048@students.del.ac.id', 'aktif', 'Pematang Siantar', '1999-07-20', 'user', 'IMG_3074.JPG'),
(16, 'Josua Barimbing', 'Josua', 'josua', '11318060', '32TI2', '2018', '			Balige		', 'if318060@students.del.ac.id', '', 'Balige', '2000-08-07', 'user', 'IMG_2991.JPG'),
(17, 'Wenny Siagian', 'wenny', 'wenny', '11317054', '33TI2', '2017', 'Silaen		', 'if317054@students.del.ac.id', 'aktif', 'Silaen', '0000-00-00', 'user', 'IMG_2988.JPG'),
(18, 'Mutiara Simamora', 'mutiara', 'mutiara', '11317050', '33TI2', '2017', '			Medan						', 'if317050@students.del.ac.id', 'aktif', 'Medan', '1999-05-20', 'user', 'danila.JPG'),
(19, 'Ayu Lumbantobing', 'ayu', 'ayu', '11317060', '33TI2', '2017', '			Tarutung		', 'if317060@students.del.ac.id', 'aktif', 'Tarutung', '1998-05-05', 'user', 'WhatsApp Image 2020-01-15 at 22.16.44 (1).jpeg'),
(20, 'Sylvia Kornelina Sihombing', 'sylvia', 'sylvia123', '11317022', '33TI1', '2017', '												Tarutung								', 'if317022@students.del.ac.id', 'tidak aktif', 'Tarutung', '1998-12-09', 'admin', '421c8108cfac759d262052bf103b71ea.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengumuman`
--

CREATE TABLE `pengumuman` (
  `pengumuman_id` int(11) NOT NULL,
  `bph_id` int(11) DEFAULT NULL,
  `periode_id` int(11) DEFAULT NULL,
  `detail_pengumuman` text,
  `tanggal_akhir_pengumuman` date DEFAULT NULL,
  `judul_pengumuman` varchar(225) NOT NULL,
  `foto_pengumuman` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pengumuman`
--

INSERT INTO `pengumuman` (`pengumuman_id`, `bph_id`, `periode_id`, `detail_pengumuman`, `tanggal_akhir_pengumuman`, `judul_pengumuman`, `foto_pengumuman`) VALUES
(3, NULL, NULL, 'Dear Mahasiswa,\r\n\r\nBerikut ini kami sampaikan Surat WR I tentang Batas Akhir Pelaksanaan Tugas Akhir Mahasiswa IT Del Tahun Ajaran 2019/2020 di Institut Teknologi Del.\r\n\r\nDemikian Informasi ini kami sampaikan.	\r\n							', '2020-01-29', '[Surat Edaran WR I] Batas Akhir Pelaksanaan Tugas Akhir Mahasiswa IT Del Tahun Ajaran 2019/2020 di Institut Teknologi Del', 'tiga.png'),
(4, 3, 1, 'asdfg', '2020-07-24', 'asdfg', 'a.jpeg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `periode`
--

CREATE TABLE `periode` (
  `periode_id` int(11) NOT NULL,
  `periode_awal` varchar(100) DEFAULT NULL,
  `periode_akhir` varchar(100) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `periode`
--

INSERT INTO `periode` (`periode_id`, `periode_awal`, `periode_akhir`, `status`) VALUES
(1, '2016', '2020', 1),
(3, '2019', '2022', 1),
(4, '2017', '2020', 1),
(7, '2018', '2021', 1),
(8, '2020', '2021', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `uang_kas`
--

CREATE TABLE `uang_kas` (
  `uang_kas_id` int(11) NOT NULL,
  `bph_id` int(11) DEFAULT NULL,
  `periode_id` int(11) DEFAULT NULL,
  `jumlah_bayar` varchar(225) DEFAULT NULL,
  `tanggal_bayar` date DEFAULT NULL,
  `tanggal_berakhir` date NOT NULL,
  `deskripsi_pembayaran` text,
  `status_pembayaran` varchar(50) DEFAULT NULL,
  `mahasiswa_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `uang_kas`
--

INSERT INTO `uang_kas` (`uang_kas_id`, `bph_id`, `periode_id`, `jumlah_bayar`, `tanggal_bayar`, `tanggal_berakhir`, `deskripsi_pembayaran`, `status_pembayaran`, `mahasiswa_id`) VALUES
(5, 3, 1, '20.000', '2020-07-02', '2020-08-02', 'Bayar Juli', 'Lunas', 20),
(6, 3, 1, 'Rp.456.000', '2020-09-01', '2020-10-01', 'asdfg', 'Sudah Bayar', 15),
(7, 3, 1, 'Rp.678.000', '2020-07-17', '2020-08-17', 'Uang kas Bulan Juli', 'Belum Bayar', 18);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `username` varchar(225) NOT NULL,
  `password` varchar(225) NOT NULL,
  `role` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`user_id`, `username`, `password`, `role`) VALUES
(1, 'hernan', 'hernan123', 'admin'),
(2, 'layla', 'layla123', 'admin'),
(3, 'roma', 'roma123', 'user');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bph`
--
ALTER TABLE `bph`
  ADD PRIMARY KEY (`bph_id`),
  ADD KEY `periode_id` (`periode_id`),
  ADD KEY `mahasiswa_id` (`mahasiswa_id`);

--
-- Indexes for table `kegiatan`
--
ALTER TABLE `kegiatan`
  ADD PRIMARY KEY (`kegiatan_id`),
  ADD KEY `bph_id` (`bph_id`),
  ADD KEY `periode_id` (`periode_id`);

--
-- Indexes for table `kuisioner`
--
ALTER TABLE `kuisioner`
  ADD PRIMARY KEY (`kuisioner_id`),
  ADD KEY `bph_id` (`bph_id`),
  ADD KEY `kegiatan_id` (`kegiatan_id`);

--
-- Indexes for table `kuisioner_kegiatan`
--
ALTER TABLE `kuisioner_kegiatan`
  ADD PRIMARY KEY (`kuisioner_id`);

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`mahasiswa_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengumuman`
--
ALTER TABLE `pengumuman`
  ADD PRIMARY KEY (`pengumuman_id`),
  ADD KEY `bph_id` (`bph_id`),
  ADD KEY `periode_id` (`periode_id`);

--
-- Indexes for table `periode`
--
ALTER TABLE `periode`
  ADD PRIMARY KEY (`periode_id`);

--
-- Indexes for table `uang_kas`
--
ALTER TABLE `uang_kas`
  ADD PRIMARY KEY (`uang_kas_id`),
  ADD KEY `bph_id` (`bph_id`),
  ADD KEY `periode_id` (`periode_id`),
  ADD KEY `mahasiswa_id` (`mahasiswa_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bph`
--
ALTER TABLE `bph`
  MODIFY `bph_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `kegiatan`
--
ALTER TABLE `kegiatan`
  MODIFY `kegiatan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `kuisioner`
--
ALTER TABLE `kuisioner`
  MODIFY `kuisioner_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `kuisioner_kegiatan`
--
ALTER TABLE `kuisioner_kegiatan`
  MODIFY `kuisioner_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  MODIFY `mahasiswa_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pengumuman`
--
ALTER TABLE `pengumuman`
  MODIFY `pengumuman_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `periode`
--
ALTER TABLE `periode`
  MODIFY `periode_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `uang_kas`
--
ALTER TABLE `uang_kas`
  MODIFY `uang_kas_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `bph`
--
ALTER TABLE `bph`
  ADD CONSTRAINT `bph_ibfk_1` FOREIGN KEY (`periode_id`) REFERENCES `periode` (`periode_id`),
  ADD CONSTRAINT `bph_ibfk_2` FOREIGN KEY (`mahasiswa_id`) REFERENCES `mahasiswa` (`mahasiswa_id`);

--
-- Ketidakleluasaan untuk tabel `kegiatan`
--
ALTER TABLE `kegiatan`
  ADD CONSTRAINT `kegiatan_ibfk_1` FOREIGN KEY (`bph_id`) REFERENCES `bph` (`bph_id`),
  ADD CONSTRAINT `kegiatan_ibfk_2` FOREIGN KEY (`periode_id`) REFERENCES `periode` (`periode_id`);

--
-- Ketidakleluasaan untuk tabel `kuisioner`
--
ALTER TABLE `kuisioner`
  ADD CONSTRAINT `kuisioner_ibfk_1` FOREIGN KEY (`bph_id`) REFERENCES `bph` (`bph_id`),
  ADD CONSTRAINT `kuisioner_ibfk_2` FOREIGN KEY (`kegiatan_id`) REFERENCES `kegiatan` (`kegiatan_id`);

--
-- Ketidakleluasaan untuk tabel `pengumuman`
--
ALTER TABLE `pengumuman`
  ADD CONSTRAINT `pengumuman_ibfk_1` FOREIGN KEY (`bph_id`) REFERENCES `bph` (`bph_id`),
  ADD CONSTRAINT `pengumuman_ibfk_2` FOREIGN KEY (`periode_id`) REFERENCES `periode` (`periode_id`);

--
-- Ketidakleluasaan untuk tabel `uang_kas`
--
ALTER TABLE `uang_kas`
  ADD CONSTRAINT `uang_kas_ibfk_1` FOREIGN KEY (`bph_id`) REFERENCES `bph` (`bph_id`),
  ADD CONSTRAINT `uang_kas_ibfk_2` FOREIGN KEY (`periode_id`) REFERENCES `periode` (`periode_id`),
  ADD CONSTRAINT `uang_kas_ibfk_3` FOREIGN KEY (`mahasiswa_id`) REFERENCES `mahasiswa` (`mahasiswa_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
